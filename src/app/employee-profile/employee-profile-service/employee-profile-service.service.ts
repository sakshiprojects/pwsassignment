import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { EmployeeProfileDataRequestModel } from '../employee-profile-data/employee-profile-model';

@Injectable({
  providedIn: 'root',
})
export class EmployeeProfileServiceService {
  constructor(private http: HttpClient, private baseUrl: BaseUrl) {}

  createAndStoreEmployee(employeeData: EmployeeProfileDataRequestModel) {
    return this.http.post(this.baseUrl.baseURL, employeeData);
  }

  getLocationService(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (response) => {
          console.log(response);
          resolve({
            longitude: response.coords.longitude,
            latitude: response.coords.latitude,
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  getReverseGeoCode(lat, lng) {
    let url = `https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${lat}&longitude=${lng}&localityLanguage=en`;
    return this.http.get(url);
  }
}

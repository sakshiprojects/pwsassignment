import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";
import { EmployeeProfileDataRequestModel } from "./employee-profile-data/employee-profile-model";
import { EmployeeProfileServiceService } from "./employee-profile-service/employee-profile-service.service";

@Component({
  selector: "app-employee-profile",
  templateUrl: "./employee-profile.component.html",
  styleUrls: ["./employee-profile.component.css"],
})
export class EmployeeProfileComponent implements OnInit {
  genders = ["male", "female"];
  maritalStatus = ["single", "married"];
  employeeForm: FormGroup;
  public isLoading = false;
  public employeeData: EmployeeProfileDataRequestModel;
  public imagePath;
  public latitude;
  public longitude;
  public address;
  public image;
  public fileName;
  public contentType;
  public base64textString;
  public error = new Subject<string>();

  constructor(
    private router: Router,
    private employeeProfileService: EmployeeProfileServiceService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.employeeForm = new FormGroup({
      employeeName: new FormControl(null, [Validators.required]),
      emailID: new FormControl(null, [Validators.required, Validators.email]),
      phoneNo: new FormControl(null, [
        Validators.required,
        Validators.pattern("^((\\+91-?) |0)?[0-9]{10}$"),
      ]),
      uploadImage: new FormControl(this.image, [Validators.required]),
      designation: new FormControl(null, [Validators.required]),
      gender: new FormControl("female"),
      maritalStatus: new FormControl("single"),
      latitude: new FormControl({ value: this.latitude, disabled: true }),
      longitude: new FormControl({ value: this.longitude, disabled: true }),
      address: new FormControl({ value: this.address, disabled: true }),
      description: new FormControl(null, [Validators.required]),
    });
  }

  onAddEmployee(employeeData) {
    this.isLoading = true;
    employeeData.latitude = this.latitude;
    employeeData.longitude = this.longitude;
    employeeData.address = this.address;
    employeeData.profile = this.base64textString;
    console.log(employeeData);

    this.employeeProfileService.createAndStoreEmployee(employeeData).subscribe(
      (response) => {
        this.isLoading = false;
        console.log(response);
        alert("Employee Data Added Successfully");
      },
      (error) => {
        this.error.next(error.message);
      }
    );
    this.employeeForm.reset();
  }

  getLocation() {
    this.employeeProfileService.getLocationService().then((response) => {
      this.latitude = response.latitude;
      this.longitude = response.longitude;
      console.log(response.longitude);
      console.log(response.latitude);
      this.getReverseGeoLocation(response.latitude, response.longitude);
    });
  }

  getReverseGeoLocation(lat, lng) {
    this.employeeProfileService
      .getReverseGeoCode(lat, lng)
      .subscribe((response: any) => {
        console.log(response);
        let { locality } = response;
        this.address = locality;
        console.log(this.address);
      });
  }

  onFileSelected(event): void {
    console.log(event.target.files[0]);
    const files = event.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this.handleFile.bind(this);
      reader.readAsBinaryString(file);
      this.fileName = file.name;
      this.contentType = file.type;
    }
  }

  handleFile(event) {
    var binaryString = event.target.result;
    this.base64textString = btoa(binaryString);
    console.log(this.base64textString);
  }

  onFetchEmployee(): void {
    this.router.navigate(["/employeeList"]);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { EmployeeDetailsComponent } from './employee-list/employee-details/employee-details.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeProfileComponent,
    EmployeeListComponent,
    LoadingSpinnerComponent,
    EmployeeDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    AgmCoreModule.forRoot({
      // apiKey: 'AIzaSyAFgM81Qz-SwfTzUsr4F51AgDj0HdN88CQ',
      apiKey: 'AIzaSyC0_lSMC4WVhB9TA6woLEHWhj7BdJYgtyE',
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { EmployeeListDataRequestModel } from '../employee-list-data/employee-list-model';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EmployeeListServiceService {
  constructor(private http: HttpClient, private baseUrl: BaseUrl) {}

  fetchEmployeeList() {
    return this.http
      .get<{ [key: string]: EmployeeListDataRequestModel }>(
        this.baseUrl.baseURL
      )
      .pipe(
        map((responseData) => {
          const listArray: EmployeeListDataRequestModel[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              listArray.push({ ...responseData[key], id: key });
            }
          }
          return listArray;
        }),
        catchError((errorRes) => {
          return throwError(errorRes);
        })
      );
  }
}

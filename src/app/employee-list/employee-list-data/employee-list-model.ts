export class EmployeeListDataRequestModel {
  public id?: string;
  public employeeName: string;
  public emailID: string;
  public phoneNo: number;
  public profile: string;
  public uploadImage: string;
  public designation: string;
  public gender: string;
  public maritalStatus: string;
  public latitude: number;
  public longitude: number;
  public address: string;
  public description: string;
}

import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { EmployeeDetailsDataRequestModel } from "./employee-details/employee-details-data/employee-details-model";
import { EmployeeListDataRequestModel } from "./employee-list-data/employee-list-model";
import { EmployeeListServiceService } from "./employee-list-service/employee-list-service.service";

@Component({
  selector: "app-employee-list",
  templateUrl: "./employee-list.component.html",
  styleUrls: ["./employee-list.component.css"],
})
export class EmployeeListComponent implements OnInit {
  public isLoading = false;
  error = null;
  public imagePath;
  listEmpData = [];
  public empProfile: EmployeeListDataRequestModel;

  constructor(
    private employeeListService: EmployeeListServiceService,
    private location: Location,
    private router: Router,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.onFetchEmployeeList();
  }

  onClickEmployeeName(item): void {
    localStorage.setItem("employeeData", JSON.stringify(item));
    this.router.navigate(["employeeList/employeeDetails"], {
      skipLocationChange: false,
    });
    this.location.replaceState("/employeeDetails");
  }

  onFetchEmployeeList() {
    this.isLoading = true;
    this.employeeListService.fetchEmployeeList().subscribe(
      (response) => {
        this.isLoading = false;
        if (response) {
          const empListArr: EmployeeListDataRequestModel[] = [];
          if (response) {
            response.forEach((element) => {
              const empData: EmployeeListDataRequestModel = new EmployeeDetailsDataRequestModel();
              empData.id = element.id;
              empData.employeeName = element.employeeName;
              empData.emailID = element.emailID;
              empData.phoneNo = element.phoneNo;
              empData.uploadImage = element.uploadImage;
              empData.profile = "data:image/jpg;base64," + element.profile;
              empData.designation = element.designation;
              empData.gender = element.gender;
              empData.maritalStatus = element.maritalStatus;
              empData.latitude = element.latitude;
              empData.longitude = element.longitude;
              empData.address = element.address;
              empData.description = element.description;
              empListArr.push(empData);
            });
          }
          this.listEmpData = empListArr;
        }
      },
      (error) => {
        this.isLoading = false;
        this.error = error.message;
        console.log(error);
      }
    );
  }

  backToEmployeeForm(): void {
    this.location.replaceState("/");
  }
}

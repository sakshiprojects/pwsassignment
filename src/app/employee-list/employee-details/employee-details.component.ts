import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { EmployeeDetailsDataRequestModel } from './employee-details-data/employee-details-model';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css'],
})
export class EmployeeDetailsComponent implements OnInit {
  public employeeDetailsData;
  public isLoading = false;
  public employeeID;
  public employeeName;
  public latitude;
  public longitude;
  public imagePath;
  // latitude = 12.9716;
  // longitude = 77.5946;

  constructor(private location: Location, private _sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.onGetEmployeeDetails();
  }

  onGetEmployeeDetails(): void {
    this.isLoading = true;
    const empDetailsData = JSON.parse(localStorage.getItem('employeeData'));

    const employeeDetailsData: EmployeeDetailsDataRequestModel = new EmployeeDetailsDataRequestModel();
    if (empDetailsData) {
      this.isLoading = false;
      employeeDetailsData.employeeName = empDetailsData.employeeName;
      employeeDetailsData.emailID = empDetailsData.emailID;
      employeeDetailsData.phoneNo = empDetailsData.phoneNo;
      employeeDetailsData.uploadImage = empDetailsData.uploadImage;
      this.imagePath = empDetailsData.profile;

      console.log(empDetailsData);
      // employeeDetailsData.uploadImage = empDetailsData.uploadImage;
      employeeDetailsData.designation = empDetailsData.designation;
      employeeDetailsData.gender = empDetailsData.gender;
      employeeDetailsData.maritalStatus = empDetailsData.maritalStatus;
      employeeDetailsData.address = empDetailsData.address;
      employeeDetailsData.latitude = empDetailsData.latitude;
      employeeDetailsData.longitude = empDetailsData.longitude;
      employeeDetailsData.description = empDetailsData.description;
    }
    this.employeeDetailsData = employeeDetailsData;
  }

  backToEmployeeList(): void {
    this.location.replaceState('/employeeList');
  }

  backToEmployeeForm(): void {
    this.location.replaceState('/');
  }
}
